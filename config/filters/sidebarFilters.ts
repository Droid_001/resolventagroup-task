import { FilterItemInterface } from "@/types/SidebarFilter";

export const characterFilters: Array<FilterItemInterface> = [
    {
        title: "Пол",
        key: "gender",
        options: [
            {
                value: "female",
                text: "Женский",
            },
            {
                value: "male",
                text: "Мужской",
            },
            {
                value: "genderless",
                text: "Бесполый",
            },
            {
                value: "unknown",
                text: "Неизвестно",
            },
        ],        
    },
    {
        title: "Статус",
        key: "status",
        options: [
            {
                value: "alive",
                text: "Жив",
            },
            {
                value: "dead",
                text: "Скончался",
            },
            {
                value: "unknown",
                text: "Неизвестно",
            },
        ],        
    },
];