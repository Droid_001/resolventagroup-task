import { Store } from "vuex";
import { getModule } from "vuex-module-decorators";
import RickAndMorty from "@/store/api/RickAndMorty";
import GlobalNotifications from "~/store/layout/GlobalNotifications";

let rickAndMortyStore: RickAndMorty
let globalNotificationsStore: GlobalNotifications

const initialiseStores = (store: Store<any>) => {
    rickAndMortyStore = getModule(RickAndMorty, store);
    globalNotificationsStore = getModule(GlobalNotifications, store);
}

export { initialiseStores, rickAndMortyStore, globalNotificationsStore };
