import { Module, VuexModule, Mutation } from "vuex-module-decorators";
import { GlobalNotificationsInterface } from "@/types/GlobalNotification";

@Module({
    name: "layout/GlobalNotifications",
    stateFactory: true,
    namespaced: true,
})
export default class GlobalNotifications extends VuexModule {
    private lastNotification: GlobalNotificationsInterface = {} as GlobalNotificationsInterface;

    get LAST_NOTIFICATION(): GlobalNotificationsInterface {
        return this.lastNotification;
    }

    @Mutation
    SHOW_NOTIFICATION(note: GlobalNotificationsInterface): void {
        this.lastNotification = note;
    }
}
