import { Module, VuexModule, Mutation, Action } from "vuex-module-decorators";

import { $axios } from "@/utils/api";

import { CharacterDto, InfoDto } from "@/Dto/CharacterDto";
import { GlobalNotificationsTypes } from "@/types/GlobalNotification";

import { getCharacters } from "@/config/api/index";

import { globalNotificationsStore } from "@/store";

@Module({
    name: "api/RickAndMorty",
    stateFactory: true,
    namespaced: true,
})
export default class RickAndMorty extends VuexModule {
    private charactersList: { results: Array<CharacterDto>; info: InfoDto } = {
        results: [],
        info: {
            count: 0,
            pages: 0,
        },
    };

    get CHARACTERS_LIST(): { results: Array<CharacterDto>; info: InfoDto } {
        return this.charactersList;
    }

    @Mutation
    SET_CHARACTERS_LIST(data: {
        results: Array<CharacterDto>;
        info: InfoDto;
    }): void {
        data.results.forEach((character) => {
            character.favorite = false;
        });
        this.charactersList = {
            results: data.results,
            info: {
                count: data.info.count,
                pages: data.info.pages,
            },
        };
    }

    @Mutation
    CHECK_ITEM_FAVORITE(item: CharacterDto) {
        item.favorite = !item.favorite;
    }

    @Action
    async LOAD_CHARACTERS_LIST(query: string = ""): Promise<void> {
        try {
            // вообще я делаю надстройку на axios с валидациями,
            // но т.к. проект не боевой, я этого делать не стану)
            const data: any = await $axios.$get(getCharacters + query);
            this.SET_CHARACTERS_LIST({
                info: data.info,
                results: data.results,
            });
        } catch (err) {
            this.SET_CHARACTERS_LIST({
                info: {
                    pages: 0,
                    count: 0,
                },
                results: [],
            });
            console.error(err);
            globalNotificationsStore.SHOW_NOTIFICATION({
                tilte:
                    (err && err.response && err.response.status
                        ? err.response.status
                        : ""),
                message:
                    err && err.response && err.response.data
                        ? err.response.data.error
                        : "Ошибка сети",
                type: GlobalNotificationsTypes.alert,
            });
        }
    }
}
