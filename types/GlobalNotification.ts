export enum GlobalNotificationsTypes {
    success = 'success',
    alert = 'danger',
    info ='info'
}

export interface GlobalNotificationsInterface {
    tilte:string,
    message:string,
    type: GlobalNotificationsTypes
}