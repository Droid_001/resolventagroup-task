export interface FilterOptionInterface {
    text: string
    value: string
}

export interface FilterItemInterface {
    title: string
    key: string
    options: Array<FilterOptionInterface>;
}