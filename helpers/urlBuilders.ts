export function buildQueryParams(params: {
    [key: string]: string | number;
}): string {
    let result = "";

    for (let key in params) {
        if (params[key]) result += `${key}=${params[key]}&`;
    }

    if (result.length) result = "?" + result.substr(0, result.length - 1);

    return result;
}
