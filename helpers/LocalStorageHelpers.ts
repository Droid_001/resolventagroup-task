export const getFromLocalStorage = (key: string, json: boolean = true): any => {
    try {
        return json
            ? JSON.parse(String(localStorage.getItem(key)))
            : localStorage.getItem(key);
    } catch (e) {
        return null;
    }
};

export const setToLocalStorage = (
    key: string,
    value: any,
    json: boolean = true
): any => {
    try {
        json
            ? localStorage.setItem(key, JSON.stringify(value))
            : localStorage.setItem(key, value);
    } catch (e) {
        return null;
    }
};
