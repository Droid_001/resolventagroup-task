import {
    validate,
    IsString,
    IsUrl,
    IsArray,
    ValidateNested,
    IsNumber,
    IsBoolean,
} from "class-validator";

export class InfoDto {
    @IsNumber()
    count: number;
    @IsNumber()
    pages: number;
    @IsString()
    next?: string;
    @IsString()
    prev?: string;
}

export class originDto {
    @IsString()
    name: string;
    @IsUrl()
    url: string;
}

export class locationDto {
    @IsString()
    name: string;
    @IsUrl()
    url: string;
}

export class CharacterDto {
    @IsNumber()
    id: number;
    @IsString()
    name: string;
    @IsString()
    status: string;
    @IsString()
    species: string;
    @IsString()
    type: string;
    @IsString()
    gender: string;
    @ValidateNested()
    origin: originDto;
    @ValidateNested()
    location: locationDto;
    @IsUrl()
    image: string;
    @IsArray()
    episode: Array<string>;
    @IsBoolean()
    favorite?: boolean;
}
